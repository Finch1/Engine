#pragma once
#include<texture.hpp>
#include<node.hpp>
#include<cstring>
#include<iostream>
namespace Engine{
    class Image:public Node{
    public:
        Image(const std::string & filePath);
        void Setup(Shader & shader);
        void Render();
        GLuint GetTBO();
    private:
        float vertices[20] = {
            // positions             // texture coords
             0.5f,  0.5f, 0.0f,      1.0f, 1.0f, // top right
             0.5f, -0.5f, 0.0f,      1.0f, 0.0f, // bottom right
            -0.5f, -0.5f, 0.0f,      0.0f, 0.0f, // bottom left
            -0.5f,  0.5f, 0.0f,      0.0f, 1.0f  // top left 
        };
        unsigned int indices[6] = {  
            3, 0, 1, // first triangle
            1, 2, 3  // second triangle
        };
        int m_width,m_height,m_nrChannels;
        GLuint TBO;
    };
}