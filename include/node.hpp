#include<glad/glad.h>
#include<shader.hpp>
#include<list>
#pragma once
namespace Engine{
    class Node{
    public:
        virtual void Setup(Shader & shader);
        virtual void Render();
        void AddChild(Node *object);
        ~Node();
    protected:
        void SetIndicesNumber(int indicesNumber);
        void SetVisible(bool visible);
    protected:
        GLuint VAO,VBO,EBO;
        unsigned int m_indicesNumber;
        Shader *m_shader;
        std::list<Node*> m_nodes;
        bool m_visible;
    };
}