#define STB_IMAGE_IMPLEMENTATION
#include<director.hpp>
#include<image.hpp>
#include<model.h>
int main(){
    Engine::Director m_direc(1000,1000,"hello");

    //Engine::Image img("resources/picture/0.png");
    //Engine::Image img2("resources/picture/a.png");
    //m_direc.AddChild(&img);
    //m_direc.AddChild(&img2);
    Engine::Model model("resources/model/shuijin.FBX",true);
    glm::mat4 mat = glm::mat4(1.0f);
    mat = glm::scale(mat,glm::vec3(0.8,0.8,0.8));
    mat = glm::rotate(mat,3.14f,glm::vec3(0.0,0.0,1.0));
    mat = glm::translate(mat, glm::vec3(0.0f, 1.2f, 0.0f)); // translate it down so it's at the center of the scene
    m_direc.getShader().bind("model",mat);

    while(!m_direc.ShouldClose()){
        m_direc.Clear();
        m_direc.Render();
        model.Draw(m_direc.getShader());
        m_direc.Update();
    }
}