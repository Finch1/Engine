#pragma once
#include<node.hpp>
namespace Engine{
    void Node::Setup(Shader & shader){
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);
        m_shader = &shader;
    }
    void Node::Render(){
        if(m_visible){
            m_shader->activate();
            glBindVertexArray(VAO);
            glDrawElements(GL_TRIANGLES, m_indicesNumber, GL_UNSIGNED_INT, 0);
        }
        for(Node * i : m_nodes){
            if(i && i->m_visible)i->Render();
        }
    }
    void Node::SetIndicesNumber(int indicesNumber){
        m_indicesNumber = indicesNumber;
    }
    void Node::AddChild(Node *object){
        object->Setup(*m_shader);
        m_nodes.push_back(object);
    }
    void Node::SetVisible(bool visible){
        m_visible = visible;
    }
    Node::~Node(){
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
        glDeleteBuffers(1, &EBO);
    }
}