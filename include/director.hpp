#pragma once
#include<cstring>
#include<glad/glad.h>
#include<GLFW/glfw3.h>
#include<shader.hpp>
#include<node.hpp>
#include<list>
using namespace std;

namespace Engine{
    class Director{
    public:
        Director(int width,int height,string title);
        ~Director();
        bool ShouldClose();
        void Clear();
        void Update();
        void Render();
        void AddChild(Node *object);
        Shader & getShader();
    private:
        Shader m_shader;
        int m_width,m_height;
        GLFWwindow *m_window;
        list<Node*> m_nodes;
    };
}